﻿Cos'è e come si effettua la taratura statica?

{%$]----------

La taratura è un procedimento che consiste nel confronto tra un dispositivo di misura ed un secondo avente caratteristiche metrologiche adeguatamente superiori, al fine di ricavare l'insieme di dati di correzione e l'incertezza del dispositivo di misura in taratura. 

Il suo scopo è quello di qualificare il sistema di misura, legando la grandezza di ingresso con quella di uscita: serve a trovare il legame tra il valore letto sullo strumento e il valore della grandezza fisica misurata.

Per tarare lo strumento di misura in modo corretto occorre far variare la grandezza campione su tutto il campo do misura dello strumento da tarare.

Per prima cosa bisogna acquisire N dati, ottenuti conoscendo il misurando, e riportando sul grafico i valori letti dallo strumento. Per interpolare i punti, al fine di trarne una relazione matematica lineare (o polinomiale), si utilizza il metodo dei minimi quadrati. Indice della bontà del modello scelto è il coefficiente di correlazione r^2. È importante inoltre analizzare la distribuzione dei residui, che non deve avere una particolare tendenza, ma deve essere del tutto casuale. Bisogna infine valutare l'incertezza sia legata al riferimento (che deve essere trascurabile), sia all'errore di misura dello strumento, per poter così delineare il diagramma di taratura.
